package com.devcamp.musicapi.musicapi.model;

public class CComposer extends CPerson {
    private String stagename;

    public CComposer(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public CComposer(String firstname, String lastname, String stagename) {
        super(firstname, lastname);
        this.stagename = stagename;
    }

    public String getStagename() {
        return stagename;
    }

    public void setStagename(String stagename) {
        this.stagename = stagename;
    }

    
}
