package com.devcamp.musicapi.musicapi.model;

import java.util.List;

public class CArtist extends CComposer {
    private List<CAlbum> albums;

    public CArtist(String firstname, String lastname, String stagename, List<CAlbum> albums) {
        super(firstname, lastname, stagename);
        this.albums = albums;
    }

    public CArtist(String firstname, String lastname, String stagename) {
        super(firstname, lastname, stagename);
    }

    public List<CAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(List<CAlbum> albums) {
        this.albums = albums;
    }
}
