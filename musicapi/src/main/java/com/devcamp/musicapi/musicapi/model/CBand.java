package com.devcamp.musicapi.musicapi.model;

import java.util.List;

public class CBand {
    private String bandname;
    private List<CBandMember> members;
    private List<CAlbum> albums;

    public CBand(String bandname, List<CBandMember> members, List<CAlbum> albums) {
        this.bandname = bandname;
        this.members = members;
        this.albums = albums;
    }

    public String getBandname() {
        return bandname;
    }

    public void setBandname(String bandname) {
        this.bandname = bandname;
    }

    public List<CBandMember> getMembers() {
        return members;
    }

    public void setMembers(List<CBandMember> members) {
        this.members = members;
    }

    public List<CAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(List<CAlbum> albums) {
        this.albums = albums;
    }

    public void addAlbum(CAlbum album) {
        this.albums.add(album);
    }
}
