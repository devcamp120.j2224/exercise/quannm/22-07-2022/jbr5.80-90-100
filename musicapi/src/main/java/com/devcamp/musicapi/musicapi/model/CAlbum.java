package com.devcamp.musicapi.musicapi.model;

import java.util.List;

public class CAlbum {
    private String name;
    private List<String> songs;
    
    public CAlbum(String name, List<String> songs) {
        this.name = name;
        this.songs = songs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSongs() {
        return songs;
    }

    public void setSongs(List<String> songs) {
        this.songs = songs;
    }
}
