package com.devcamp.musicapi.musicapi.service;

import java.util.ArrayList;

import com.devcamp.musicapi.musicapi.model.CAlbum;
import com.devcamp.musicapi.musicapi.model.CArtist;
import com.devcamp.musicapi.musicapi.model.CBand;
import com.devcamp.musicapi.musicapi.model.CBandMember;
import com.devcamp.musicapi.musicapi.model.CComposer;

public class CComposerService {
    ArrayList<CAlbum> albums = new CAlbumService().getAllAlbum();

    CArtist artist1 = new CArtist("Nguyễn", "An", "1");
    CArtist artist2 = new CArtist("Huỳnh", "Phong", "2");
    CArtist artist3 = new CArtist("Phạm", "Linh", "3");

    CBandMember member1 = new CBandMember("Hoàng", "Long", "4", "rock");
    CBandMember member2 = new CBandMember("Hồ", "Ân", "5", "rap");
    CBandMember member3 = new CBandMember("Vũ", "Phương", "6", "ballad");
    CBandMember member4 = new CBandMember("Bùi", "Tùng", "7", "r&b");
    CBandMember member5 = new CBandMember("Từ", "Từ", "8", "dance");
    CBandMember member6 = new CBandMember("Đoàn", "Phương", "9", "bolero");

    CAlbumService albumService = new CAlbumService();

    public ArrayList<CArtist> getAllArtist() {
        artist1.setAlbums(albumService.getListAlbumA());
        artist2.setAlbums(albumService.getListAlbumB());
        artist3.setAlbums(albumService.getListAlbumC());
        ArrayList<CArtist> listArtist = new ArrayList<>();
        listArtist.add(artist1);
        listArtist.add(artist3);
        listArtist.add(artist2);
        return listArtist;
    }

    public ArrayList<CBand> getAllBand() {
        ArrayList<CBandMember> bandMembers1 = new ArrayList<>();
        ArrayList<CBandMember> bandMembers2 = new ArrayList<>();
        ArrayList<CBandMember> bandMembers3 = new ArrayList<>();
        bandMembers1.add(member1);
        bandMembers1.add(member3);
        bandMembers2.add(member4);
        bandMembers2.add(member5);
        bandMembers3.add(member2);
        bandMembers3.add(member6);
        CBand band1 = new CBand("Band01", bandMembers1, albumService.getListAlbumA());
        CBand band2 = new CBand("Band02", bandMembers2, albumService.getListAlbumB());
        CBand band3 = new CBand("Band03", bandMembers3, albumService.getListAlbumC());
        ArrayList<CBand> listBands = new ArrayList<>();
        listBands.add(band1);
        listBands.add(band2);
        listBands.add(band3);
        return listBands;
    }
    
    public ArrayList<CComposer> getAllComposer() {
        ArrayList<CComposer> listComposers = new ArrayList<>();
        listComposers.addAll(getAllArtist());
        listComposers.add(member1);
        listComposers.add(member3);
        listComposers.add(member4);
        listComposers.add(member2);
        listComposers.add(member6);
        listComposers.add(member2);
        return listComposers;
    }
}
