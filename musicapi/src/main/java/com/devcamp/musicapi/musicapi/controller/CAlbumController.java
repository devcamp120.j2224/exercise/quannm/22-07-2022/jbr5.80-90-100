package com.devcamp.musicapi.musicapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.musicapi.model.CAlbum;
import com.devcamp.musicapi.musicapi.service.CAlbumService;

@CrossOrigin
@RestController
public class CAlbumController {
    @GetMapping("/albums")
    public ArrayList<CAlbum> getAlbums() {
        return new CAlbumService().getAllAlbum();
    }
}
