package com.devcamp.musicapi.musicapi.service;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.musicapi.musicapi.model.CAlbum;

public class CAlbumService {
    public ArrayList<CAlbum> getListAlbumA() {
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        List<String> list3 = new ArrayList<>();
        list1.add("song 1a, song 1b");
        list2.add("song 2a, song 2b");
        list3.add("song 3a, song 3b");
        CAlbum album1 = new CAlbum("Album01", list1);
        CAlbum album2 = new CAlbum("Album02", list2);
        CAlbum album3 = new CAlbum("Album03", list3);
        ArrayList<CAlbum> listAlbumA = new ArrayList<>();
        listAlbumA.add(album1);
        listAlbumA.add(album2);
        listAlbumA.add(album3);
        return listAlbumA;
    }

    public ArrayList<CAlbum> getListAlbumB() {
        List<String> list4 = new ArrayList<>();
        List<String> list5 = new ArrayList<>();
        List<String> list6 = new ArrayList<>();
        list4.add("song 4a, song 4b");
        list5.add("song 5a, song 5b");
        list6.add("song 6a, song 6b");
        CAlbum album4 = new CAlbum("Album04", list4);
        CAlbum album5 = new CAlbum("Album05", list5);
        CAlbum album6 = new CAlbum("Album06", list6);
        ArrayList<CAlbum> listAlbumB = new ArrayList<>();
        listAlbumB.add(album4);
        listAlbumB.add(album5);
        listAlbumB.add(album6);
        return listAlbumB;
    }

    public ArrayList<CAlbum> getListAlbumC() {
        List<String> list7 = new ArrayList<>();
        List<String> list8 = new ArrayList<>();
        List<String> list9 = new ArrayList<>();
        List<String> list10 = new ArrayList<>();
        list7.add("song 7a, song 7b");
        list8.add("song 8a, song 8b");
        list9.add("song 9a, song 9b");
        list10.add("song 10a, song 10b");
        CAlbum album7 = new CAlbum("Album07", list7);
        CAlbum album8 = new CAlbum("Album08", list8);
        CAlbum album9 = new CAlbum("Album09", list9);
        CAlbum album10 = new CAlbum("Album10", list10);
        ArrayList<CAlbum> listAlbumC = new ArrayList<>();
        listAlbumC.add(album7);
        listAlbumC.add(album8);
        listAlbumC.add(album9);
        listAlbumC.add(album10);
        return listAlbumC;
    }
    public ArrayList<CAlbum> getAllAlbum() {
        ArrayList<CAlbum> listAll = new ArrayList<>();
        listAll.addAll(getListAlbumA());
        listAll.addAll(getListAlbumB());
        listAll.addAll(getListAlbumC());
        return listAll;
    }
}
