package com.devcamp.musicapi.musicapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.musicapi.musicapi.model.CArtist;
import com.devcamp.musicapi.musicapi.model.CBand;
import com.devcamp.musicapi.musicapi.model.CComposer;
import com.devcamp.musicapi.musicapi.service.CComposerService;

@CrossOrigin
@RestController
public class CComposerController {
    @GetMapping("/bands")
    public ArrayList<CBand> getBands() {
        return new CComposerService().getAllBand();
    }

    @GetMapping("/artists")
    public ArrayList<CArtist> getArtists() {
        return new CComposerService().getAllArtist();
    }

    @GetMapping("/composers")
    public ArrayList<CComposer> getComposers() {
        return new CComposerService().getAllComposer();
    }
}
