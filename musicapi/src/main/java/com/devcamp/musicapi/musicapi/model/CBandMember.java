package com.devcamp.musicapi.musicapi.model;

public class CBandMember extends CComposer {
    private String instrument;

    public CBandMember(String firstname, String lastname, String stagename, String instrument) {
        super(firstname, lastname, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }
}
